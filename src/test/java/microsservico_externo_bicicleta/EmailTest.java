package microsservico_externo_bicicleta;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmailTest {

	private Email email;
	
	@BeforeEach                                         
    public void setUp() throws Exception {
        email  = new Email();
    }
	
	//Testa o lancamento da excecao caso o endereco de email esteja incorreto
	@Test
	public void testExcecaoAddressException() throws AddressException{
		email.setEmail("henrique.gmail.com");
		email.setMensagem("Teste");
		
		Exception exception = assertThrows(MessagingException.class, () -> {
			Email.enviarEmail(email);
	    });	
	}

}
