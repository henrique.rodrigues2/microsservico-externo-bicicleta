package microsservico_externo_bicicleta;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import kong.unirest.json.JSONObject;

class CiclistaTest {

	private Ciclista ciclista;
	
	@BeforeEach                                         
    public void setUp() throws Exception {
        ciclista  = new Ciclista();
    }
	
	//Testa se o nome do ciclista retornado e Anna Bia
	@Test
	public void testFuncaoBuscarCiclistaIntegracao() {
		ciclista = Ciclista.buscarCiclistaIntegracao("cc93687a-c5ce-408e-bc27-dd7597439615");
		assertEquals("Anna Bia", ciclista.getNome());
	}
	
	//Testa se a resposta retornada nao esta vazia
	@Test
	public void testMicrosservicoCiclistaNotNull() throws UnirestException{
		String id = "cc93687a-c5ce-408e-bc27-dd7597439615";
		HttpResponse<JsonNode> resposta = Unirest.get("https://grupo3-aluguel.herokuapp.com/ciclista/{idCiclista}")
				  .header("accept", "application/json")
				  .routeParam("idCiclista", id)
			      .asJson();	
		
		assertNotNull(resposta.getBody());
	}
	
	//Testa se o status do servico e 200
	@Test
	public void testMicrosservicoCiclistaStatus() throws UnirestException{
		String id = "cc93687a-c5ce-408e-bc27-dd7597439615";
		HttpResponse<JsonNode> resposta = Unirest.get("https://grupo3-aluguel.herokuapp.com/ciclista/{idCiclista}")
				  .header("accept", "application/json")
			      .routeParam("idCiclista", id)
			      .asJson();	
		
		assertEquals(200, resposta.getStatus());
	}
	
}
